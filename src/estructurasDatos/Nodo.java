package estructurasDatos;

public class Nodo {
    private String item;
    private Nodo sig;
    private Nodo ant;
    
    public Nodo(String item){
        this.item = item;
        this.sig = null;
        this.ant = null;
    }

    public String getItem() {
        return item;
    }

    public void setItem(String item) {
        this.item = item;
    }

    public Nodo getSig() {
        return sig;
    }

    public void setSig(Nodo sig) {
        this.sig = sig;
    }

    public Nodo getAnt() {
        return ant;
    }

    public void setAnt(Nodo ant) {
        this.ant = ant;
    }
    
}
