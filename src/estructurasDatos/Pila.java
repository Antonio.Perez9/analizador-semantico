package estructurasDatos;

public class Pila {

    private Nodo tope;

    public Pila() {
        this.tope = null;
    }

    public void push(String item) {
        Nodo nuevo = new Nodo(item);
        if (this.isEmpty()) {
            this.tope = nuevo;
        } else {
            this.tope.setSig(nuevo);
            nuevo.setAnt(tope);
            this.tope = nuevo;
        }
    }

    public void pop() {
        if (!this.isEmpty()) {
            if (this.tope.getAnt() == null) {
                this.tope = null;
            } else {
                Nodo aux = this.tope.getAnt();
                aux.setSig(null);
                this.tope.setAnt(null);
                this.tope = aux;
            }
        }
    }

    public boolean isEmpty() {
        return (this.tope == null) ? true : false;
    }
}
