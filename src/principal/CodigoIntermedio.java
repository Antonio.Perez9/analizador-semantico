package principal;

import java.util.ArrayList;
import estructurasDatos.Pila;

public class CodigoIntermedio {

    private final String[] codigo;
    private final ArrayList<String> codigoSalida;
    private int i;
    private int conTemp;
    private int contEtq;
    private final ArrayList<ArrayList<String[]>> cuadruples;

    public CodigoIntermedio(String codigo[]) {
        this.codigo = codigo;
        this.codigoSalida = new ArrayList<>();
        this.i = 0;
        this.conTemp = 1;
        this.contEtq = 10;
        this.cuadruples = new ArrayList<>();
    }

    public ArrayList<String> generarCI() {
        for (i = 0; i < codigo.length - 1; i++) {   //Recorrido del codigo
            this.opcionesCodigoInter();
        }
        return codigoSalida;
    }

    private void codigoEstructuras() {
        Pila pila = new Pila();
        pila.push("{");
        while (!pila.isEmpty()) {
            i++;
            if (codigo[i].equals("}")) {
                pila.pop();
            }
            if (codigo[i + 1] != null && !pila.isEmpty()) {
                this.opcionesCodigoInter();
            }
        }
    }

    private void opcionesCodigoInter() {
        if (codigo[i + 1].equals("=")) {    //Caso donde encuentra una asignacion
            String var;
            if (codigo[i].charAt(0) == 't') {
                var = "$" + codigo[i];
            } else {
                var = codigo[i];
            }
            i += 2;
            String aux[] = this.obtenerCodigo(";");     //Obtiene el codigo hasta el ;
            if (aux.length > 1) {   //Es una asignacion con operaciones
                this.descomponerOperacion(aux, var);
            } else {    //Solo es una asignacion simple
                String linea = var + " " + codigo[i - 2] + " " + codigo[i - 1];
                codigoSalida.add(linea);
            }

        } else if (codigo[i].equals("if")) {   //If y If Else
            if (this.isIfElse(i)) {   //If Else

                String etqVer = "E" + contEtq;
                contEtq += 10;
                String etqFal = "E" + contEtq;
                contEtq += 10;
                String s_Sig = "E" + contEtq;
                contEtq += 10;
                i += 1;
                String aux[] = this.obtenerCodigo("{");
                this.generarECodigo(aux, etqVer, etqFal, aux.length - 1); //Genera el E Codigo de la condicion dada
                codigoSalida.add(etqVer + ":");
                this.codigoEstructuras(); //Recursion para analizar lo de adentro S1.Codigo     
                codigoSalida.add("goto " + s_Sig);
                codigoSalida.add(etqFal + ":");
                while (!codigo[i].equals("{")) {
                    i++;
                }
                this.codigoEstructuras(); //S2.Codigo 
                codigoSalida.add(s_Sig + ":");

            } else {  //Solo If
                String etqVer = "E" + contEtq;
                contEtq += 10;
                String etqFal = "E" + contEtq;
                contEtq += 10;
                i += 1;
                String aux[] = this.obtenerCodigo("{");
                this.generarECodigo(aux, etqVer, etqFal, aux.length - 1); //Genera el E Codigo de la condicion dada
                codigoSalida.add(etqVer + ":");
                this.codigoEstructuras(); //Recursion para analizar lo de adentro S1.Codigo
                codigoSalida.add(etqFal + ":");
            }

        } else if (codigo[i].equals("while")) { //Ciclo While
            String s_Com = "E" + contEtq;
            contEtq += 10;
            String etqVer = "E" + contEtq;
            contEtq += 10;
            String etqFal = "E" + contEtq;
            contEtq += 10;
            codigoSalida.add(s_Com + ":");
            i += 1;
            String aux[] = this.obtenerCodigo("{");
            this.generarECodigo(aux, etqVer, etqFal, aux.length - 1); //Genera el E Codigo de la condicion dada
            codigoSalida.add(etqVer + ":");
            this.codigoEstructuras(); //Recursion para analizar lo de adentro S1.Codigo
            codigoSalida.add("goto " + s_Com);
            codigoSalida.add(etqFal + ":");

        } else if (codigo[i].equals("num") || codigo[i].equals("str") || codigo[i].equals("show") || codigo[i].equals("input")) {    //Declaracion, impresion, lectura
            String aux[] = this.obtenerCodigo(";");
            String linea = "";
            for (int j = 0; j < aux.length; j++) {
                if (aux[0].equals("num") || aux[0].equals("str")) {
                    linea += aux[j] + " ";
                } else {
                    linea += aux[j];
                }
            }
            codigoSalida.add(linea);
        }
    }

    private void generarECodigo(String[] cod, String etqV, String etqF, int ind) { //Este es pura recursion ALV
        ind--;
        ArrayList<String> aux = new ArrayList<>();
        while (!cod[ind].equals("|") && !cod[ind].equals("&") && !cod[ind].equals("(")) {
            aux.add(cod[ind]);
            ind--;
        }
        String condSimple = "";
        for (int j = aux.size() - 1; j >= 0; j--) {
            condSimple += aux.get(j) + " ";
        }
        String E1Ver, E1Fal, E2Ver, E2Fal;
        switch (cod[ind]) {
            case "|":
                E1Ver = etqV;
                E1Fal = "E" + contEtq;
                contEtq += 10;
                E2Ver = etqV;
                E2Fal = etqF;
                this.generarECodigo(cod, E1Ver, E1Fal, ind);
                codigoSalida.add(E1Fal + ":");
                codigoSalida.add("if " + condSimple + "goto " + E2Ver);
                codigoSalida.add("goto " + E2Fal);
                break;
            case "&":
                E1Ver = "E" + contEtq;
                contEtq += 10;
                E1Fal = etqF;
                E2Ver = etqV;
                E2Fal = etqF;
                this.generarECodigo(cod, E1Ver, E1Fal, ind);
                codigoSalida.add(E1Ver + ":");
                codigoSalida.add("if " + condSimple + "goto " + E2Ver);
                codigoSalida.add("goto " + E2Fal);
                break;
            case "(":
                codigoSalida.add("if " + condSimple + "goto " + etqV);
                codigoSalida.add("goto " + etqF);
                break;
        }
    }

    private boolean isIfElse(int z) { //Verifica si es IF solo o con Else
        Pila pila = new Pila();
        while (!codigo[z].equals("{")) {
            z++;
        }
        pila.push("{");
        while (!codigo[z].equals("}") || !pila.isEmpty()) {
            z++;
            if (codigo[z].equals("{")) {
                pila.push("{");
            } else if (codigo[z].equals("}")) {
                pila.pop();
            }
        }
        if (codigo[z + 1] != null) {
            if (codigo[z + 1].equals("else")) {
                return true;
            }
        }
        return false;
    }

    private void descomponerOperacion(String[] op, String varP) {   //Genera operaciones simples de una mas compleja
        String[] opCom = op;
        ArrayList<String[]> cuadruple = new ArrayList<>();
        conTemp = 1;
        for (int j = 0; j < opCom.length; j++) {    //Primero analiza ^
            if (opCom[j].equals("^")) {
                this.generarTemporal(opCom, j, cuadruple);
                opCom = this.redimencionarArreglo(opCom, j);
                j = 0;
            }
        }
        for (int j = 0; j < opCom.length; j++) {    //Luego analiza * /
            if (opCom[j].equals("*") || opCom[j].equals("/")) {
                this.generarTemporal(opCom, j, cuadruple);
                opCom = this.redimencionarArreglo(opCom, j);
                j = 0;
            }
        }
        for (int j = 0; j < opCom.length; j++) {    //Luego + -
            if (opCom[j].equals("+") || opCom[j].equals("-")) {
                this.generarTemporal(opCom, j, cuadruple);
                opCom = this.redimencionarArreglo(opCom, j);
                j = 0;
            }
        }
        String linea = varP + " = " + opCom[0];   //Por ultimo hace la asignacion
        codigoSalida.add(linea);
        String fila[] = new String[4];
        fila[0] = opCom[0];
        fila[1] = "=";
        fila[2] = "null";
        fila[3] = varP;
        cuadruple.add(fila);

        this.cuadruples.add(cuadruple);
    }

    private void generarTemporal(String[] opC, int j, ArrayList<String[]> cua) {    //Genera los temporales   
        String linea = "t" + conTemp +  " = " + opC[j - 1] + opC[j] + opC[j + 1];
        codigoSalida.add(linea);
        String fila[] = new String[4];
        fila[0] = opC[j - 1];
        fila[2] = opC[j + 1];
        fila[1] = opC[j];
        fila[3] = "t" + conTemp;
        cua.add(fila);
    }

    private String[] redimencionarArreglo(String[] opCom, int j) {  //Redimenciona el arreglo ya con el temporal agregado
        int l = 0;
        String[] aux = opCom;
        opCom = new String[aux.length - 2];
        for (int k = 0; k < opCom.length; k++) {
            if (k == j - 1) {
                opCom[k] = "t" + conTemp;
                l += 3;
            } else {
                opCom[k] = aux[l];
                l++;
            }
        }
        conTemp++;
        return opCom;
    }

    private String[] obtenerCodigo(String tope) {   //Obtiene el codigo hasta un tope dado
        ArrayList<String> aux = new ArrayList<>();
        while (!codigo[i].equals(tope)) {
            aux.add(codigo[i]);
            i++;
        }
        String salida[] = new String[aux.size()];
        for (int j = 0; j < salida.length; j++) {
            salida[j] = aux.get(j);
        }
        return salida;
    }

    public ArrayList<ArrayList<String[]>> getCuadruples() {
        return cuadruples;
    }
}
