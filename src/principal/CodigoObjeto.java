package principal;

import java.util.ArrayList;
import java.util.StringTokenizer;

public class CodigoObjeto {

    private final ArrayList<String> codigoIntermedio;
    private final ArrayList<String> codigoObj;
    private final Simbolo[] tablaSimbolos;
    private final ArrayList<String> temporales;

    public CodigoObjeto(ArrayList<String> codigoIntermedio, Simbolo[] tablaSimbolos, ArrayList<String> temporales) {
        this.codigoIntermedio = codigoIntermedio;
        this.codigoObj = new ArrayList<>();
        this.tablaSimbolos = tablaSimbolos;
        this.temporales = temporales;
        System.out.println(temporales);
    }

    public void generarCodigoObjeto() {
        ArrayList<String> data = new ArrayList<>();
        ArrayList<String> code = new ArrayList<>();
        ArrayList<String> msjs = new ArrayList<>();
        Lexico lx = new Lexico();
        int numMsj = 0;
        int contAjs = 0;
        int etCon = 0;
        for (int i = 0; i < temporales.size(); i++) {
            data.add(temporales.get(i) + " dw " + " 0");
        }
        for (int i = 0; i < codigoIntermedio.size(); i++) {
            StringTokenizer st = new StringTokenizer(codigoIntermedio.get(i), "()+-*/^\" ", true);
            ArrayList<String> linea = this.guardarTokens(st);
            System.out.println(linea);
            int pos;
            switch (linea.get(0)) {
                case "num": //Creacion de variables
                    data.add(linea.get(1) + " dw " + " 0");
                    break;
                case "str":
                    data.add(linea.get(1) + " dw 25 dup('$')");
                    break;
                case "show": //Escritura                   
                    for (int j = 1; j < linea.size(); j++) {
                        lx = lx.Etiquetar(linea.get(j));
                        if (lx.numero == 51) { //Si la salida es una cadena de texto
                            msjs.add("msj" + numMsj + " dw " + linea.get(j) + ",'$'");
                            code.add("mov ah,09h");
                            code.add("mov dx,offset " + "msj" + numMsj);
                            code.add("int 21h");
                            numMsj++;
                        } else if (lx.numero == 52) {
                            pos = buscarVariable(linea.get(j));
                            if (pos != -1) {
                                if (tablaSimbolos[pos].getTipo().equals("num")) {
                                    code.add("mov ax," + linea.get(j));
                                    code.add("lea dx,buffer");
                                    code.add("call cifToConcar");

                                    code.add("mov dx,offset buffer");
                                    code.add("mov ah,09h");
                                    code.add("int 21h");
                                }else{
                                    code.add("mov dx,offset " + linea.get(j));
                                    code.add("mov ah,09h");
                                    code.add("int 21h");
                                }
                            }
                        }
                    }
                    code.add("call saltoLinea");
                    code.add("");
                    break;

                case "input": //Lectura
                    code.add("mov dx,offset buffer");
                    code.add("mov ah,0ah");
                    code.add("int 21h");
                    code.add("call saltoLinea");
                    code.add("");
                    pos = buscarVariable(linea.get(1));
                    if (pos != -1) {
                        if (tablaSimbolos[pos].getTipo().equals("num")) {
                            code.add("lea dx,buffer + 2");
                            code.add("call concarToCif");
                            code.add("mov " + linea.get(1) + ",ax");
                        } else {
                            code.add("mov si,2");
                            code.add("ajustarCadena" + contAjs + ":");
                            code.add("mov dx, buffer+si");
                            code.add("mov " + linea.get(1) + "[si-2],dx");
                            code.add("inc si");
                            code.add("cmp dx,36");
                            code.add("ja ajustarCadena" + contAjs);
                            code.add("jb ajustarCadena" + contAjs);
                            contAjs++;
                        }
                    }
                    code.add("");
                    break;
                case "if": //Comparacion
                    code.add("mov ax," + linea.get(1));
                    code.add("cmp " + "ax," + linea.get(3));
                    switch (linea.get(2)) {
                        case "<":
                            code.add("jb " + linea.get(5));
                            break;
                        case "<=":
                            code.add("jbe " + linea.get(5));
                            break;
                        case ">":
                            code.add("ja " + linea.get(5));
                            break;
                        case ">=":
                            code.add("jae " + linea.get(5));
                            break;
                        case "==":
                            code.add("je " + linea.get(5));
                            break;
                        case "!":
                            code.add("jne " + linea.get(5));
                            break;
                    }
                    break;
                case "goto": //Salto incondicional
                    code.add("jmp " + linea.get(1));
                    break;
                default:
                    switch (linea.size()) {
                        case 1: //Etiqueta
                            code.add(linea.get(0));
                            break;
                        case 3: //Asignacion
                            lx = lx.Etiquetar(linea.get(2));
                            switch (lx.numero) {
                                case 52: //Variable variable
                                    pos = buscarVariable(linea.get(2));
                                    if (pos != -1) {
                                        if (tablaSimbolos[pos].getTipo().equals("str")) {
                                            code.add("mov si,0");
                                            code.add("ajustarCadena" + contAjs + ":");
                                            code.add("mov dx," + linea.get(2) + "si");
                                            code.add("mov " + linea.get(0) + "[si],dx");
                                            code.add("inc si");
                                            code.add("cmp dx,36");
                                            code.add("ja ajustarCadena" + contAjs);
                                            code.add("jb ajustarCadena" + contAjs);
                                            contAjs++;
                                        } else {
                                            code.add("mov dx," + linea.get(2));
                                            code.add("mov " + linea.get(0) + ",dx");
                                        }
                                    } else if (temporales.contains(linea.get(2))) {
                                        code.add("mov dx," + linea.get(2));
                                        code.add("mov " + linea.get(0) + ",dx");
                                    }

                                    break;
                                case 51://Variable Cadena
                                    data.add("msj" + numMsj + " dw 25 dup('$')");
                                    code.add("ajustarCadena" + contAjs + ":");
                                    code.add("mov dx," + "msj" + numMsj + "si");
                                    code.add("mov " + linea.get(0) + "[si],dx");
                                    code.add("inc si");
                                    code.add("cmp dx,36");
                                    code.add("ja ajustarCadena" + contAjs);
                                    code.add("jb ajustarCadena" + contAjs);
                                    contAjs++;
                                    numMsj++;
                                    break;
                                default://Variable numero
                                    code.add("mov dx," + linea.get(2));
                                    code.add("mov " + linea.get(0) + ",dx");
                                    break;
                            }
                            code.add("");
                            break;

                        case 5: //Operacion
                            switch (linea.get(3)) {
                                case "^":
                                    code.add("mov bx," + linea.get(4));
                                    code.add("cmp bx,1");
                                    code.add("jne calcular");
                                    code.add("mov " + linea.get(0) + ",1");
                                    code.add("calcular:");
                                    code.add("sub bx,1");
                                    code.add("mov si,bx");
                                    code.add("mov dx," + linea.get(2));
                                    code.add("potencia:");
                                    code.add("mov ax," + linea.get(2));
                                    code.add("mul dx");
                                    code.add("mov dx,ax");
                                    code.add("dec si");
                                    code.add("jnz potencia");
                                    code.add("mov " + linea.get(0) + ",dx");
                                    break;
                                case "*":
                                    code.add("mov ax," + linea.get(2));
                                    code.add("mov dx," + linea.get(4));
                                    code.add("mul dx");
                                    code.add("mov " + linea.get(0) + ",ax");
                                    code.add("");
                                    break;
                                case "/":
                                    code.add("mov dx,0");
                                    code.add("mov ax," + linea.get(2));
                                    code.add("mov bx," + linea.get(4));
                                    code.add("div bx");
                                    code.add("mov " + linea.get(0) + ",ax");
                                    code.add("");
                                    break;
                                case "+":
                                    code.add("mov ax," + linea.get(2));
                                    code.add("add ax," + linea.get(4));
                                    code.add("mov " + linea.get(0) + ",ax");
                                    code.add("");
                                    break;
                                case "-":
                                    code.add("mov ax," + linea.get(2));
                                    code.add("sub ax," + linea.get(4));
                                    code.add("mov " + linea.get(0) + ",ax");
                                    code.add("jns continuar" + etCon);
                                    code.add("mov " + linea.get(0) + ",0");
                                    code.add("continuar"+etCon+":");
                                    code.add("");
                                    etCon++;
                                    break;
                            }
                            break;
                    }
                    break;
            }
        }
        codigoObj.add(".model small");
        codigoObj.add(".stack");
        codigoObj.add("dw 64 dup(0)");
        codigoObj.add(".data");
        codigoObj.add("buffer dw 128 dup('$')");
        for (int i = 0; i < data.size(); i++) {
            codigoObj.add(data.get(i));
        }
        for (int i = 0; i < msjs.size(); i++) {
            codigoObj.add(msjs.get(i));
        }
        codigoObj.add(".code");
        codigoObj.add("start:");
        codigoObj.add("");
        codigoObj.add("mov ax,@data");
        codigoObj.add("mov ds,ax");
        codigoObj.add("mov es,ax");
        for (int i = 0; i < code.size(); i++) {
            codigoObj.add(code.get(i));
        }
        codigoObj.add(".exit");

        codigoObj.add("saltoLinea proc");
        codigoObj.add("mov ah,02h");
        codigoObj.add("mov dl,10");
        codigoObj.add("int 21h");
        codigoObj.add("mov ah,02h");
        codigoObj.add("mov dl,13");
        codigoObj.add("int 21h");
        codigoObj.add("ret");
        
        codigoObj.add("cifToConcar proc");
        codigoObj.add("push ax");
        codigoObj.add("push bx");
        codigoObj.add("push cx");
        codigoObj.add("push dx");
        codigoObj.add("push di");
        
        codigoObj.add("mov bx,10");
        codigoObj.add("mov di,dx");
        codigoObj.add("xor cx,cx");
        
        
        codigoObj.add("divisiones:");
        codigoObj.add("xor dx,dx");
        codigoObj.add("div bx");
        codigoObj.add("add dl,'0'");
        codigoObj.add("push dx");
        codigoObj.add("inc cx");
        codigoObj.add("cmp ax,0");
        codigoObj.add("jne divisiones");
     
        codigoObj.add("invertir:");
        codigoObj.add("pop [di]");
        codigoObj.add("inc di");
        codigoObj.add("loop invertir");
        codigoObj.add("mov [di],'$'");
        codigoObj.add("pop di");
        codigoObj.add("pop dx");
        codigoObj.add("pop cx");
        codigoObj.add("pop bx");
        codigoObj.add("pop ax");
        codigoObj.add("ret");
        
        codigoObj.add("concarToCif proc");
        codigoObj.add("push bx");
        codigoObj.add("push cx");
        codigoObj.add("push dx");
        codigoObj.add("push si");
        
        codigoObj.add("mov si,dx");
        codigoObj.add("xor ax,ax");
        codigoObj.add("mov bx,10");
        
        codigoObj.add("conv:");
        codigoObj.add("mov cl,[si]");
        codigoObj.add("cmp cl,13");
        codigoObj.add("je finCad");
        codigoObj.add("cmp cl,'$'");
        codigoObj.add("je finCad");
        
        codigoObj.add("mul bx");
        codigoObj.add("sub cl,'0'");
        codigoObj.add("xor ch,ch");
        codigoObj.add("add ax,cx");
        codigoObj.add("inc si");
        codigoObj.add("jmp conv");
        
        codigoObj.add("finCad:");
        codigoObj.add("pop si");
        codigoObj.add("pop dx");
        codigoObj.add("pop cx");
        codigoObj.add("pop bx");
        codigoObj.add("ret");
     
                
        
    }

    private ArrayList<String> guardarTokens(StringTokenizer st) {
        ArrayList<String> cod = new ArrayList<>();
        while (st.hasMoreElements()) {
            String token = st.nextToken();
            if (token.equals("\"")) {
                String aux = "";
                aux = aux + token;
                while (st.hasMoreElements()) {
                    token = st.nextToken();
                    if (token.equals("\"")) {
                        aux = aux + token;
                        break;
                    }
                    aux = aux + token;
                }
                cod.add(aux);
            } else if (token.charAt(0) == '$') {
                cod.add(token.substring(1));
            } else if (!token.equals(" ") && !token.equals("(") && !token.equals(")")) {
                cod.add(token);
            }
        }
        return cod;
    }

    private int buscarVariable(String nombre) {
        for (int i = 0; i < tablaSimbolos.length; i++) {
            if (nombre.equals(tablaSimbolos[i].getNombreVar())) {
                return i;
            }
        }
        return -1;
    }

    public ArrayList<String> getCodigoObj() {
        return codigoObj;
    }

}
