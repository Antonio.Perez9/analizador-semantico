package principal;

import java.io.IOException;
import java.util.ArrayList;
import java.util.StringTokenizer;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;

/**
 *
 *
 */
public class InterfazPrincipal extends javax.swing.JFrame {

    int vecSal[];
    String codigoTokens[];
    Semantico semantico;
    CodigoIntermedio ci;
    ArrayList<String> codigoIntermedio;
    ArrayList<String> codigoOp;
    OptimizacionCodigo opc;

    public InterfazPrincipal() {
        initComponents();
        setTitle("Semantico");
        
        
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        pnContenedor = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        pnLexico = new javax.swing.JPanel();
        lbLexico = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        jTProgramaFuente = new javax.swing.JTextArea();
        jScrollPane2 = new javax.swing.JScrollPane();
        jTProgramaCompilado = new javax.swing.JTextArea();
        jPanelCargar = new javax.swing.JPanel();
        lblCargar = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jButton1 = new javax.swing.JButton();
        jButton2 = new javax.swing.JButton();
        objectButton = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("LOS PEÑAS");
        setResizable(false);

        pnContenedor.setBackground(new java.awt.Color(255, 204, 255));
        pnContenedor.setName(""); // NOI18N
        pnContenedor.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jLabel1.setFont(new java.awt.Font("Microsoft Tai Le", 1, 20)); // NOI18N
        jLabel1.setText("Source Code");
        pnContenedor.add(jLabel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(80, 20, -1, -1));

        jLabel2.setFont(new java.awt.Font("Microsoft Tai Le", 1, 20)); // NOI18N
        jLabel2.setText(" Semantic");
        pnContenedor.add(jLabel2, new org.netbeans.lib.awtextra.AbsoluteConstraints(360, 20, -1, 20));

        pnLexico.setBackground(new java.awt.Color(255, 153, 255));
        pnLexico.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));

        lbLexico.setFont(new java.awt.Font("Microsoft Tai Le", 1, 14)); // NOI18N
        lbLexico.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lbLexico.setText("Run!! :3");
        lbLexico.setToolTipText("");
        lbLexico.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        lbLexico.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                lbLexicoMouseClicked(evt);
            }
        });

        javax.swing.GroupLayout pnLexicoLayout = new javax.swing.GroupLayout(pnLexico);
        pnLexico.setLayout(pnLexicoLayout);
        pnLexicoLayout.setHorizontalGroup(
            pnLexicoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, pnLexicoLayout.createSequentialGroup()
                .addGap(0, 0, Short.MAX_VALUE)
                .addComponent(lbLexico, javax.swing.GroupLayout.PREFERRED_SIZE, 70, javax.swing.GroupLayout.PREFERRED_SIZE))
        );
        pnLexicoLayout.setVerticalGroup(
            pnLexicoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, pnLexicoLayout.createSequentialGroup()
                .addGap(0, 0, Short.MAX_VALUE)
                .addComponent(lbLexico, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE))
        );

        pnContenedor.add(pnLexico, new org.netbeans.lib.awtextra.AbsoluteConstraints(180, 370, 70, 30));

        jTProgramaFuente.setColumns(20);
        jTProgramaFuente.setFont(new java.awt.Font("Segoe UI", 0, 14)); // NOI18N
        jTProgramaFuente.setRows(5);
        jTProgramaFuente.setBorder(new javax.swing.border.MatteBorder(null));
        jTProgramaFuente.setMargin(new java.awt.Insets(2, 10, 2, 6));
        jScrollPane1.setViewportView(jTProgramaFuente);

        pnContenedor.add(jScrollPane1, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 60, 270, 270));

        jTProgramaCompilado.setColumns(20);
        jTProgramaCompilado.setFont(new java.awt.Font("Segoe UI", 0, 14)); // NOI18N
        jTProgramaCompilado.setRows(5);
        jTProgramaCompilado.setBorder(new javax.swing.border.MatteBorder(null));
        jScrollPane2.setViewportView(jTProgramaCompilado);

        pnContenedor.add(jScrollPane2, new org.netbeans.lib.awtextra.AbsoluteConstraints(300, 60, 260, 270));

        jPanelCargar.setBackground(new java.awt.Color(255, 153, 255));
        jPanelCargar.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));

        lblCargar.setFont(new java.awt.Font("Microsoft Tai Le", 1, 14)); // NOI18N
        lblCargar.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lblCargar.setText("Code");
        lblCargar.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                lblCargarMouseClicked(evt);
            }
        });

        javax.swing.GroupLayout jPanelCargarLayout = new javax.swing.GroupLayout(jPanelCargar);
        jPanelCargar.setLayout(jPanelCargarLayout);
        jPanelCargarLayout.setHorizontalGroup(
            jPanelCargarLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanelCargarLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(lblCargar, javax.swing.GroupLayout.PREFERRED_SIZE, 39, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(33, Short.MAX_VALUE))
        );
        jPanelCargarLayout.setVerticalGroup(
            jPanelCargarLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanelCargarLayout.createSequentialGroup()
                .addGap(0, 0, Short.MAX_VALUE)
                .addComponent(lblCargar, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE))
        );

        pnContenedor.add(jPanelCargar, new org.netbeans.lib.awtextra.AbsoluteConstraints(70, 370, 80, 30));

        jLabel3.setFont(new java.awt.Font("Microsoft Tai Le", 1, 14)); // NOI18N
        jLabel3.setText("By: Automatitos brillantes");
        pnContenedor.add(jLabel3, new org.netbeans.lib.awtextra.AbsoluteConstraints(370, 400, 180, 40));

        jButton1.setBackground(new java.awt.Color(255, 153, 255));
        jButton1.setFont(new java.awt.Font("Microsoft Tai Le", 0, 12)); // NOI18N
        jButton1.setText("Codigo Intermedio");
        jButton1.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                buttonCodigoIntermedio(evt);
            }
        });
        pnContenedor.add(jButton1, new org.netbeans.lib.awtextra.AbsoluteConstraints(270, 370, 140, 30));

        jButton2.setBackground(new java.awt.Color(255, 153, 255));
        jButton2.setText("Optimizacion");
        jButton2.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        jButton2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton2ActionPerformed(evt);
            }
        });
        pnContenedor.add(jButton2, new org.netbeans.lib.awtextra.AbsoluteConstraints(420, 370, 100, 30));

        objectButton.setBackground(new java.awt.Color(255, 153, 255));
        objectButton.setText("Objeto");
        objectButton.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        objectButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                objectButtonActionPerformed(evt);
            }
        });
        pnContenedor.add(objectButton, new org.netbeans.lib.awtextra.AbsoluteConstraints(540, 370, 60, 30));

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(pnContenedor, javax.swing.GroupLayout.DEFAULT_SIZE, 628, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(pnContenedor, javax.swing.GroupLayout.PREFERRED_SIZE, 431, javax.swing.GroupLayout.PREFERRED_SIZE)
        );

        pack();
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void lbLexicoMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lbLexicoMouseClicked

       
        StringTokenizer st = new StringTokenizer(jTProgramaFuente.getText(), "+-^*/();=<>{}|&\n\r\t\",: ", true);
        String texto = "";
        String cadena = "";
        boolean bnd = true;
        while (st.hasMoreElements()) {
            cadena = st.nextToken();
            if (cadena.equals("=")) {
                texto = texto + cadena;
                if (st.hasMoreElements()) {
                    cadena = st.nextToken();
                    if (cadena.equals("=")) {//DOBLE IGUAL` 
                        texto = texto + cadena + "\n";
                    } else if (cadena.equals("\"")) {//CADENA
                        String aux = "";
                        aux = aux + "\n" + cadena;
                        while (st.hasMoreElements()) {
                            cadena = st.nextToken();
                            if (cadena.equals("\"")) {
                                aux = aux + cadena;
                                break;
                            }
                            aux = aux + cadena;
                        }
                        texto = texto + aux + "\n";
                    } else if (!(cadena.equals(" ") || cadena.equals("\t"))) {
                        texto = texto + "\n" + cadena + "\n";
                    } else {
                        texto = texto + "\n";
                    }

                }
            } else if (cadena.equals(">") || cadena.equals("<")) {
                texto = texto + cadena;
                if (st.hasMoreElements()) {
                    cadena = st.nextToken();
                    if (cadena.equals("=")) {
                        texto = texto + cadena + "\n";
                    } else if (cadena.equals("\"")) {//CADENA
                        String aux = "";
                        aux = aux + "\n" + cadena;
                        while (st.hasMoreElements()) {
                            cadena = st.nextToken();
                            if (cadena.equals("\"")) {
                                aux = aux + cadena;
                                break;
                            }
                            aux = aux + cadena;
                        }
                        texto = texto + aux + "\n";
                    } else if (!(cadena.equals(" ") || cadena.equals("\t"))) {
                        texto = texto + "\n" + cadena + "\n";
                    } else {
                        texto = texto + "\n";
                    }
                }
            } else if (cadena.equals("\"")) {//CADENA
                String aux = "";
                aux = aux + cadena;
                while (st.hasMoreElements()) {
                    cadena = st.nextToken();
                    if (cadena.equals("\"")) {
                        aux = aux + cadena;
                        break;
                    }
                    aux = aux + cadena;
                }
                texto = texto + aux + "\n";
            } else if (cadena.equals(" ") || cadena.equals("\t")) {
                cadena = "";
            } else if (cadena.equals("(") || cadena.equals(")")) {
                texto = texto + "\n" + cadena + "\n";
            } else {
                texto = texto + cadena + "\n";
            }
        }

        //}
//        String txt1=texto;
        StringTokenizer st2 = new StringTokenizer(texto, "\n\r\f");
        texto = "";
        Lexico obLex2 = new Lexico();
        vecSal = new int[st2.countTokens() + 1];
        codigoTokens = new String[st2.countTokens() + 1];
        int i = 0;
        while (st2.hasMoreElements()) {
            obLex2 = obLex2.Etiquetar(st2.nextToken());
            texto = texto + "\n" + obLex2.lexema + "\t" + obLex2.nombre + "\t" + obLex2.numero;
            vecSal[i] = obLex2.numero;
            codigoTokens[i] = obLex2.lexema;
            i++;
        }
        vecSal[vecSal.length - 1] = 53;
        jTProgramaCompilado.setText(texto);

        for (int j = 0; j < vecSal.length; j++) {
            if (vecSal[j] == 102 || vecSal[j] == 103 || vecSal[j] == 100 || vecSal[j] == 101) {
                bnd = false;
                break;
            }
        }
        if (bnd) {
            
            sintacticFun();
        } else {
            
        }
    }//GEN-LAST:event_lbLexicoMouseClicked
    
    private void sintacticFun(){
   

        String cadena = "";
        for (int i = 0; i < vecSal.length; i++) {
            cadena = cadena + vecSal[i] + " ";
        }
        cadena += "$";
        jTProgramaCompilado.setText(cadena);
        //jTProgramaFuente.setText("$ 150");
        int mg[][] = {{},
        {159, 8, 150, 7, 6, 152, 5, 3},
        {8, 150, 7, 4},
        {},
        {153, 157, 14, 157},
        {152, 13},
        {},
        {17, 52, 10},
        {17, 156, 20, 52}, //Aqui iba 22 en vez de 20 ASIGNACION
        {160, 52},
        {160, 51},
        {160, 50},
        {52},
        {51},
        {50},
        {154},
        {164},
        {162},
        {158},
        {155},
        {165},
        {150, 151},
        {},
        {160, 157, 161},
        {},
        {12},
        {20},
        {17, 6, 163, 157, 5, 1},
        {163, 157, 12}, //Aqui va 21 en vez de 12, tenia 20 CONCATENACION
        {},
        {17, 6, 52, 5, 2}, //Iba 18 en vez de 17
        {8, 150, 7, 6, 152, 5, 9},
        {50, 15},
        {}
        };
        int mt[][] = new int[17][54];
        for (int i = 0; i < 17; i++) {
            for (int j = 0; j < 53; j++) {
                mt[i][j] = -1;
            }
        }
        mt[8][3] = 1;
        mt[9][1] = 3;
        mt[9][2] = 3;
        mt[9][3] = 3;
        mt[9][4] = 2;
        mt[9][8] = 3;
        mt[9][9] = 3;
        mt[9][10] = 3;
        mt[9][52] = 3;
        mt[9][53] = 3;
        mt[2][50] = 4;
        mt[2][51] = 4;
        mt[2][52] = 4;
        mt[3][6] = 6;
        mt[3][13] = 5;
        mt[4][10] = 7;
        mt[5][52] = 8;
        mt[6][50] = 11;
        mt[6][51] = 10;
        mt[6][52] = 9;
        mt[7][50] = 14;
        mt[7][51] = 13;
        mt[7][52] = 12;
        mt[1][1] = 17;
        mt[1][2] = 16;
        mt[1][3] = 18;
        mt[1][9] = 20;
        mt[1][10] = 15;
        mt[1][52] = 19;
        mt[0][1] = 21;
        mt[0][2] = 21;
        mt[0][3] = 21;
        mt[0][8] = 22;
        mt[0][9] = 21;
        mt[0][10] = 21;
        mt[0][52] = 21;
        mt[0][53] = 22;
        mt[10][12] = 23;
        mt[10][17] = 24; //Aqui iba 18 en vez de 17  ASIGNACION
        mt[10][21] = 23;
        mt[11][12] = 25;
        mt[11][21] = 26;
        mt[12][1] = 27;
        mt[13][6] = 29;
        mt[13][12] = 28; //Aqui iba 21 en vez de 12 CONCATENACION
        mt[14][2] = 30;
        mt[15][9] = 31;

        int vecMov[] = {0, 150};
        int vecMovAux[];
        int i = 0;
        int pr = 0;
        int pc = 0;
        int nl = 0;
        int tv = 0;
        cadena = "";
        boolean b = false;
        do {
            pr = vecMov[vecMov.length - 1];
            if (pr >= 150) {
                pr = pr - 150;
                pc = vecSal[i];
                nl = mt[pr][pc];
                if (nl != -1) {
                    vecMovAux = vecMov;
                    vecMov = new int[(vecMovAux.length + mg[nl].length) - 1];
                    tv = 0;
                    for (int j = 0; j < vecMovAux.length - 1; j++) {
                        vecMov[j] = vecMovAux[j];
                    }
                    for (int j = vecMovAux.length - 1; j < vecMov.length; j++) {
                        vecMov[j] = mg[nl][tv];
                        tv++;
                    }
                    cadena = jTProgramaFuente.getText() + "\n";
                    for (int j = 0; j < vecMov.length; j++) {
                        cadena = cadena + vecMov[j] + " ";
                    }
                    //jTProgramaFuente.setText(cadena);
                    cadena = jTProgramaCompilado.getText() + "\n";
                    for (int j = i; j < vecSal.length - 1; j++) {
                        cadena = cadena + vecSal[j] + " ";
                    }
                    cadena = cadena + "$";
                    jTProgramaCompilado.setText(cadena);
                } else {
                    //cadena = jTProgramaFuente.getText() + "\n Error sintactico al recibir " + vecSal[i] + " se esperaba: ";
                    cadena = jTProgramaCompilado.getText() + "\n Error sintactico al recibir " + vecSal[i] + " se esperaba: ";
                    for (int j = 0; j < 54; j++) {
                        if (mt[pr][j] != -1) {
                            cadena = cadena + j + " ";
                        }
                    }
                    b = true;
                    //jTProgramaFuente.setText(cadena);
                    jTProgramaCompilado.setText(cadena);
                    break;
                }
            } else {
                if (vecSal[i] == vecMov[vecMov.length - 1]) {
                    i++;
                    vecMovAux = vecMov;
                    vecMov = new int[vecMovAux.length - 1];
                    for (int j = 0; j < vecMov.length; j++) {
                        vecMov[j] = vecMovAux[j];
                    }
                    cadena = jTProgramaFuente.getText() + "\n";
                    for (int j = 0; j < vecMov.length; j++) {
                        cadena = cadena + vecMov[j] + " ";
                    }
                    //jTProgramaFuente.setText(cadena);

                    cadena = jTProgramaCompilado.getText() + "\n";
                    for (int j = i; j < vecSal.length - 1; j++) {
                        cadena = cadena + vecSal[j] + " ";
                    }
                    cadena = cadena + "$";
                    jTProgramaCompilado.setText(cadena);
                } else {
                    if (vecSal[i] == 53) {
                        break;
                    }
                    int n = 0;
                    Lexico obLex = new Lexico();
                    while (n < obLex.dic.length) {
                        if (Integer.toString(vecSal[i]).equals(obLex.dic[n][2])) {
                            obLex.nombre = obLex.dic[n][1];
                            break;
                        } else if (vecSal[i] == 50) {
                            obLex.nombre = "Numerico";
                            break;
                        } else if (vecSal[i] == 51) {
                            obLex.nombre = "Cadena";
                            break;
                        } else if (vecSal[i] == 52) {
                            obLex.nombre = "Variable";
                            break;
                        }
                        n++;
                    }
                    //cadena = jTProgramaFuente.getText() + "\nError sintactico al recibir: " + obLex.nombre;
                    cadena = jTProgramaCompilado.getText() + "\nError sintactico al recibir: " + obLex.nombre;
                    n = 0;
                    b = true;
                    while (n < obLex.dic.length) {
                        if (Integer.toString(vecMov[vecMov.length - 1]).equals(obLex.dic[n][2])) {
                            obLex.nombre = obLex.dic[n][1];
                            break;
                        } else if (vecMov[vecMov.length - 1] == 50) {
                            obLex.nombre = "Numerico";
                            break;
                        } else if (vecMov[vecMov.length - 1] == 51) {
                            obLex.nombre = "Cadena";
                            break;
                        } else if (vecMov[vecMov.length - 1] == 52) {
                            obLex.nombre = "Variable";
                            break;
                        }
                        n++;
                    }
                    cadena = cadena + "\nSe esperaba: " + obLex.nombre;
                    //jTProgramaFuente.setText(cadena);
                    jTProgramaCompilado.setText(cadena);
                    break;
                }

            }
        } while (i < vecSal.length);
        if (vecMov[vecMov.length - 1] == 0) {
            //cadena = jTProgramaFuente.getText() + "\nSintacticamente Correcto";
            //jTProgramaFuente.setText(cadena);
            cadena = jTProgramaCompilado.getText() + "\nSintacticamente Correcto";
            jTProgramaCompilado.setText(cadena);
             semanticFun();
        } else if (b) {
            //cadena = jTProgramaFuente.getText() + "\nSintacticamente Incorrecto";
            //jTProgramaFuente.setText(cadena);
            //cadena = jTProgramaCompilado.getText() + "\nSintacticamente Incorrecto";
            semanticFun();
            //jTProgramaCompilado.setText(cadena);

        }
    }
    
    
    private void semanticFun(){
     this.semantico = new Semantico(codigoTokens, vecSal);
        ArrayList<String> reporte = this.semantico.analisisSemantico();
        if (reporte.isEmpty()) {
            jTProgramaCompilado.setText("Semanticamente Correcto");
        } else {
            String texto = "Semanticamente Incorrecto \n";
            for (String s : reporte) {
                texto += s + "\n";
            }
            jTProgramaCompilado.setText(texto);
        }}
    
    private void lblCargarMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lblCargarMouseClicked
        
        jTProgramaCompilado.setText(null);
        jLabel1.setText("Programa Fuente");
        jTProgramaFuente.setText(ManejoArchivos.cargarArchivo());
    }//GEN-LAST:event_lblCargarMouseClicked

    private void buttonCodigoIntermedio(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_buttonCodigoIntermedio
        this.ci = new CodigoIntermedio(codigoTokens);
        this.codigoIntermedio = this.ci.generarCI();
        String texto = "";
        for (String s : this.codigoIntermedio) {
            if (s.charAt(0) == '$') {
                texto += s.substring(1) + "\n";
            } else {
                texto += s + "\n";
            }

        }
        jTProgramaCompilado.setText(texto);
    }//GEN-LAST:event_buttonCodigoIntermedio

    private void jButton2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton2ActionPerformed
        opc = new OptimizacionCodigo(this.codigoIntermedio, this.ci.getCuadruples(),semantico.tablaSimbolos);
        this.codigoOp = opc.optimizarCodigo();
        String texto = "";
        for (String s : this.codigoOp) {
            if (s.charAt(0) == '$') {
                texto += s.substring(1) + "\n";
            } else {
                texto += s + "\n";
            }
        }
        jTProgramaCompilado.setText(texto);
    }//GEN-LAST:event_jButton2ActionPerformed

    private void objectButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_objectButtonActionPerformed
       
        CodigoObjeto co = new CodigoObjeto(codigoOp,semantico.tablaSimbolos, opc.temporales);
        co.generarCodigoObjeto();
        String texto = "";
        for (String s : co.getCodigoObj()) {
            texto += s + "\n";

        }
        jTProgramaCompilado.setText(texto);
        
        
    }//GEN-LAST:event_objectButtonActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(InterfazPrincipal.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(InterfazPrincipal.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(InterfazPrincipal.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(InterfazPrincipal.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new InterfazPrincipal().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jButton1;
    private javax.swing.JButton jButton2;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JPanel jPanelCargar;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JTextArea jTProgramaCompilado;
    private javax.swing.JTextArea jTProgramaFuente;
    private javax.swing.JLabel lbLexico;
    private javax.swing.JLabel lblCargar;
    private javax.swing.JButton objectButton;
    private javax.swing.JPanel pnContenedor;
    private javax.swing.JPanel pnLexico;
    // End of variables declaration//GEN-END:variables
}
