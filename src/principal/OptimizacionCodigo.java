package principal;

import java.util.ArrayList;

public class OptimizacionCodigo {

    private final ArrayList<ArrayList<String[]>> cuadruples;
    private final ArrayList<String> codInter;
    private final ArrayList<String> codOptimizado;
    private final Simbolo[] tablaSimbolos;
    public ArrayList<String> temporales;

    public OptimizacionCodigo(ArrayList<String> codInter, ArrayList<ArrayList<String[]>> cuadruples, Simbolo[] tablaSimbolos) {
        this.cuadruples = cuadruples;
        this.codInter = codInter;
        this.codOptimizado = new ArrayList<>();
        this.tablaSimbolos = tablaSimbolos;
        temporales = new ArrayList<>();
    }

    public ArrayList<String> optimizarCodigo() {
        int iter = 0;
        for (int i = 0; i < codInter.size(); i++) {
            if (this.codInter.get(i).charAt(0) == 't') {
                ArrayList<String[]> tabla = cuadruples.get(iter);
                i += tabla.size() - 1;
                this.optimizacion(tabla);
                iter++;
            } else {
                this.codOptimizado.add(codInter.get(i));
            }
        }
        return codOptimizado;
    }

    private void optimizacion(ArrayList<String[]> tabla) {
        /*
        0 - Operando1
        1 - Operador
        2 - Operando2
        3 - Resultado
         */

        //Operaciones repetidas
        ArrayList<String[]> aux;
        do {
            aux = (ArrayList) tabla.clone();
            for (int i = 0; i < tabla.size() - 1; i++) {
                String filaP[] = tabla.get(i);
                String opPrin = filaP[0] + filaP[1] + filaP[2];
                for (int j = i + 1; j < tabla.size() - 1; j++) {
                    String filaS[] = tabla.get(j);
                    String opSec = filaS[0] + filaS[1] + filaS[2];
                    if (opPrin.equals(opSec)) { //Compara si es igual al derecho
                        this.reemplazar(filaS[3], filaP[3], tabla);
                        tabla.remove(j);
                    } else {
                        if (filaS[1].equals("+") || filaS[1].equals("*")) {
                            opSec = filaS[2] + filaS[1] + filaS[0];
                            if (opPrin.equals(opSec)) { //Compara si es igual al reves
                                this.reemplazar(filaS[3], filaP[3], tabla);
                                tabla.remove(j);
                            }
                        }
                    }
                }
            }

            //Operaciones con constantes
            Lexico lx = new Lexico();
            for (int i = 0; i < tabla.size() - 1; i++) {
                String fila[] = tabla.get(i);
                Lexico op1 = lx.Etiquetar(fila[0]);
                Lexico op2 = lx.Etiquetar(fila[2]);
                if (op1.numero == 50 && op2.numero == 50) { //Verifica si ambos operandos son constantes
                    int o1 = Integer.parseInt(fila[0]);
                    int o2 = Integer.parseInt(fila[2]);
                    int res = 0;
                    switch (fila[1]) {
                        case "^":
                            res = ((int) Math.pow((double) o1, (double) o2));
                            break;
                        case "*":
                            res = o1 * o2;
                            break;
                        case "/":
                            if (o2 != 0) {
                                res = o1 / o2;
                            }
                            break;
                        case "+":
                            res = o1 + o2;
                            break;
                        case "-":
                            res = o1 - o2;
                            break;
                    }
                    this.reemplazar(fila[3], String.valueOf(res), tabla);
                    tabla.remove(i);
                }
            }

            //Operaciones en las cuales se conoce el resultado
            for (int i = 0; i < tabla.size() - 1; i++) {
                String fila[] = tabla.get(i);
                switch (fila[1]) {
                    case "^":
                        if (fila[0].equals("0")) { // 0^A = 0
                            this.reemplazar(fila[3], "0", tabla);
                            tabla.remove(i);
                        } else if (fila[2].equals("0")) { // A^0= 1
                            this.reemplazar(fila[3], "1", tabla);
                            tabla.remove(i);
                        } else if (fila[2].equals("1")) { // A^1= A
                            this.reemplazar(fila[3], fila[0], tabla);
                            tabla.remove(i);
                        } else if (fila[0].equals("1")) { // 1^A= 1
                            this.reemplazar(fila[3], "1", tabla);
                            tabla.remove(i);
                        }
                        break;
                    case "*":
                        if (fila[0].equals("0") || fila[2].equals("0")) { //A*0 = 0 o 0*A = 0
                            this.reemplazar(fila[3], "0", tabla);
                            tabla.remove(i);
                        } else if (fila[0].equals("1")) { // 1*A = A
                            this.reemplazar(fila[3], fila[2], tabla);
                            tabla.remove(i);
                        } else if (fila[2].equals("1")) { // A*1 = A
                            this.reemplazar(fila[3], fila[0], tabla);
                            tabla.remove(i);
                        }
                        break;
                    case "/":
                        if (fila[2].equals("1")) { //A / 1 = A
                            this.reemplazar(fila[3], fila[0], tabla);
                            tabla.remove(i);
                        } else if (fila[0].equals("0") && !fila[2].equals("0")) { //0/A =0
                            this.reemplazar(fila[3], "0", tabla);
                            tabla.remove(i);
                        } else if (fila[0].equals(fila[2])) { //A / A = 1
                            if (!fila[2].equals("0")) { // A != 0
                                this.reemplazar(fila[3], "1", tabla);
                                tabla.remove(i);
                            }
                        }
                        break;
                    case "+":
                        if (fila[0].equals("0")) { //0+A=A
                            this.reemplazar(fila[3], fila[2], tabla);
                            tabla.remove(i);
                        } else if (fila[2].equals("0")) { //A+0=A
                            this.reemplazar(fila[3], fila[0], tabla);
                            tabla.remove(i);
                        }
                        break;
                    case "-":
                        if (fila[0].equals("0")) { //0-A=-A
                            this.reemplazar(fila[3], fila[2], tabla);
                            tabla.remove(i);
                        } else if (fila[2].equals("0")) { //A-0=A
                            this.reemplazar(fila[3], fila[0], tabla);
                            tabla.remove(i);
                        } else if ((fila[0].equals(fila[2]))) { //A - A = 0
                            this.reemplazar(fila[3], "0", tabla);
                            tabla.remove(i);
                        }
                        break;
                }
            }
        } while (cambios(aux, tabla));

        ArrayList<String> temUsados = new ArrayList<>();
        for (int i = tabla.size() - 1; i >= 0; i--) {
            String tem[] = tabla.get(i);
            if (tem[0].charAt(0) == 't') {
                temUsados.add(tem[0]);
            }
            if (tem[2].charAt(0) == 't') {
                temUsados.add(tem[2]);
            }
        }

        for (int i = 0; i < temUsados.size(); i++) {
            if (!temporales.contains(temUsados.get(i))) {
                temporales.add(temUsados.get(i));
            }
        }

        for (int i = 0; i < tabla.size() - 1; i++) {
            String tem[] = tabla.get(i);
            String op, o1 = tem[0], o2 = tem[2];
            if (existe(tem[3], temUsados)) {
                if (tem[0].charAt(0) == '$') {
                    o1 = tem[0].substring(1);
                } else if (tem[2].charAt(0) == '$') {
                    o2 = tem[2].substring(1);
                }
                op = tem[3] + " = " + o1 + tem[1] + o2;

                this.codOptimizado.add(op);
            }

        }
        String tem[] = tabla.get(tabla.size() - 1);
        String o1 = tem[0];
        if (tem[0].charAt(0) == '$') {
            o1 = tem[0].substring(1);
        }
        this.codOptimizado.add(tem[3] + " = " + o1);
    }

    private void reemplazar(String reemplazado, String reemplazo, ArrayList<String[]> tabla) {
        for (int i = 0; i < tabla.size(); i++) {
            String fila[] = tabla.get(i);
            if (fila[0].equals(reemplazado)) {
                fila[0] = reemplazo;
            }
            if (fila[2].equals(reemplazado)) {
                fila[2] = reemplazo;
            }
        }
    }

    private boolean cambios(ArrayList<String[]> a1, ArrayList<String[]> a2) {
        for (int i = 0; i < a1.size(); i++) {
            String f1[] = a1.get(i);
            String f2[] = a2.get(i);
            for (int j = 0; j < f1.length; j++) {
                if (!f1[j].equals(f2[j])) {
                    return true;
                }
            }
        }
        return false;
    }

    private boolean existe(String v, ArrayList<String> tabla) {
        for (int i = 0; i < tabla.size(); i++) {
            if (v.equals(tabla.get(i))) {
                return true;
            }
        }
        return false;
    }
}
