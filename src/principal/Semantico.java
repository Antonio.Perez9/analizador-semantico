package principal;

import java.util.ArrayList;

public class Semantico {

    private final String codigo[];
    private final int noToken[];
    private ArrayList<String> errores;
    public Simbolo tablaSimbolos[];
    private int indice; //Indice de los arreglos de codigo
    private int iTabla; //Indice de la tabla de simbolos

    public Semantico(String codigo[], int noToken[]) {
        this.codigo = codigo;
        this.noToken = noToken;
        this.errores = new ArrayList<>();
        this.crearTablaSimbolos();
        this.indice = 0;
        this.iTabla = 0;
    }

    public ArrayList<String> analisisSemantico() {
        for (indice = 0; indice < noToken.length; indice++) {
            switch (noToken[indice]) {
                case 10://int y str
                    this.registroVariable();
                    break;
                case 52://VARIABLE
                    this.validarAsignacion();
                    break;
                case 9://COMPARACIONES
                case 3:
                    this.validarComparacion();
                    break;
                case 2://LEER VARIABLE
                    this.validarLectura();
                    break;
                case 1://DESPLEGAR
                    this.validarDespliegue();
                    break;
            }
        }
        for (int i = 0; i < iTabla; i++) {
            if (!this.tablaSimbolos[i].isUsada()) {
                String error = "La variable " + this.tablaSimbolos[i].getNombreVar() + " no es utilizada";
                this.errores.add(error);
            }
        }
        Simbolo aux[] = new Simbolo[iTabla];
        System.arraycopy(tablaSimbolos, 0, aux, 0, iTabla);
        tablaSimbolos = aux;
        return errores;
    }

    private void registroVariable() {
        Simbolo s = new Simbolo();
        boolean repetida = false;
        if (this.tablaSimbolos[0] == null) {//Si esta vacia entonces comenzamos
            s.setTipo(codigo[indice]);
            s.setNombreVar(codigo[++indice]);
            s.setInicializada(false);
            s.setUsada(false);

            this.tablaSimbolos[iTabla] = s;
            iTabla++;
        } else {
            for (int i = 0; i < iTabla; i++) {
                int aux = indice;
                if (codigo[++aux].equals(this.tablaSimbolos[i].getNombreVar())) {
                    String error = "La variable '" + codigo[aux] + "' esta repetida";
                    this.errores.add(error);
                    repetida = true;
                }
            }
            if (!repetida) {
                s.setTipo(codigo[indice]);
                s.setNombreVar(codigo[++indice]);
                s.setInicializada(false);
                s.setUsada(false);

                this.tablaSimbolos[iTabla] = s;
                iTabla++;
            }
        }
    }

    private void validarAsignacion() {
        ArrayList<String> instT = new ArrayList<String>();
        ArrayList<Integer> instTN = new ArrayList<Integer>();
        int t = -1;
        boolean bnd = false;
        String tipo = "";
        while (noToken[indice] != 17) {
            instT.add(codigo[indice]);
            instTN.add(noToken[indice]);
            indice++;
        }

        String vecT[] = new String[instT.size()];
        int vecTN[] = new int[instT.size()];

        // Copiar los elementos de instT a vecT
        for (int j = 0; j < instT.size(); j++) {
            vecT[j] = instT.get(j);
        }

        // Copiar los elementos de instTN a vecTN
        for (int j = 0; j < instTN.size(); j++) {
            vecTN[j] = instTN.get(j);
        }

        for (int j = 0; j < iTabla; j++) {
            if (tablaSimbolos[j].getNombreVar().equals(vecT[0])) {
                tablaSimbolos[j].setInicializada(true);
                tipo = tablaSimbolos[j].getTipo();
                bnd = true;
            }
        }

        if (bnd) {//cuando si existe la variable
            for (int j = 2; j < vecTN.length; j++) {//For para Recorrer Instrucciones   
                t = -1;
                if (tipo.equals("str")) {
                    String error;
                    switch (vecT[j]) {
                        case "-":
                            error = "La operación - con tipos de dato 'str' no es valida";
                            this.errores.add(error);
                            continue;
                        case "/":
                            error = "La operación / con tipos de dato 'str' no es valida";
                            this.errores.add(error);
                            continue;
                        case "*":
                            error = "La operación * con tipos de dato 'str' no es valida";
                            this.errores.add(error);
                            continue;
                    }
                }
                if (vecT[j].equals("/")) {
                    if(vecT[j + 1].equals("0")){
                        String error = "No es posible realizar una division entre 0";
                        this.errores.add(error);
                    }
                }
                if (vecTN[j] == 50) {//Cuando llegue un int
                    if (!tipo.equals("num")) {
                        String error = "Incompatibilidad de tipos, se recibe num se Esperaba str";
                        this.errores.add(error);
                    }
                }
                if (vecTN[j] == 51) {//Cuando llegue un str
                    if (!tipo.equals("str")) {
                        String error = "Incompatibilidad de tipos, se recibe 'str' se Esperaba num";
                        this.errores.add(error);
                    }
                }
                if (vecTN[j] == 52) {//Cuando llegue una variable
                    for (int k = 0; k < iTabla; k++) {
                        if (vecT[j].equals(tablaSimbolos[k].getNombreVar())) {
                            t = k;
                        }
                    }
                    if (t != -1) {
                        if (tablaSimbolos[t].isInicializada()) {
                            if (!tipo.equals(tablaSimbolos[t].getTipo())) {
                                String error = "Incompatibilidad de tipos, se recibe '" + tablaSimbolos[t].getTipo() + "' se esperaba " + tipo;
                                this.errores.add(error);
                            }
                            tablaSimbolos[t].setUsada(true);
                        } else {
                            String error = "La variable: " + tablaSimbolos[t].getNombreVar() + " no esta inicializada";
                            this.errores.add(error);
                        }
                    } else {
                        String error = "La variable " + vecT[j] + " no existe";
                        this.errores.add(error);
                    }
                }
            }
        } else {
            String error = "La variable " + vecT[0] + " no existe";
            this.errores.add(error);
        }
    }

    private void validarComparacion() {
        ArrayList<String> instT = new ArrayList<String>();
        ArrayList<Integer> instTN = new ArrayList<Integer>();
        int t = -1;
        while (noToken[indice] != 6) {
            instT.add(codigo[indice]);
            instTN.add(noToken[indice]);
            indice++;
        }

        String vecT[] = new String[instT.size()];
        int vecTN[] = new int[instT.size()];

        // Copiar los elementos de instT a vecT
        for (int j = 0; j < instT.size(); j++) {
            vecT[j] = instT.get(j);
        }
        // Copiar los elementos de instTN a vecTN
        for (int j = 0; j < instTN.size(); j++) {
            vecTN[j] = instTN.get(j);
        }

        for (int i = 2; i < vecTN.length; i++) {
            t = -1;
            if (vecTN[i] == 51) {
                String error = "Comparación no valida con la cadena " + vecTN[i];
                this.errores.add(error);
            }
            if (vecTN[i] == 52) {
                for (int k = 0; k < iTabla; k++) {
                    if (vecT[i].equals(tablaSimbolos[k].getNombreVar())) {
                        t = k;
                    }
                }
                if (t != -1) {
                    if (tablaSimbolos[t].isInicializada()) {
                        if (tablaSimbolos[t].getTipo().equals("str")) {
                            String error = "Comparación no valida con la variable '" + tablaSimbolos[t].getNombreVar();
                            this.errores.add(error);
                        }
                        tablaSimbolos[t].setUsada(true);
                    } else {
                        String error = "La variable: '" + tablaSimbolos[t].getNombreVar() + "' no esta inicializada";
                        this.errores.add(error);
                    }
                } else {
                    String error = "La variable '" + vecT[i] + "' no existe";
                    this.errores.add(error);
                }
            }
        }
    }

    private void validarLectura() {
        indice = indice + 2;
        int t = -1;
        for (int k = 0; k < iTabla; k++) {
            if (codigo[indice].equals(tablaSimbolos[k].getNombreVar())) {
                t = k;
            }
        }
        if (t != -1) {
            tablaSimbolos[t].setInicializada(true);
            tablaSimbolos[t].setUsada(true);
        } else {
            String error = "La variable '" + codigo[indice] + "' no existe";
            this.errores.add(error);
        }
    }

    private void validarDespliegue() {
        ArrayList<String> instT = new ArrayList<String>();
        ArrayList<Integer> instTN = new ArrayList<Integer>();
        int t = -1;
        while (noToken[indice] != 6) {
            instT.add(codigo[indice]);
            instTN.add(noToken[indice]);
            indice++;
        }

        String vecT[] = new String[instT.size()];
        int vecTN[] = new int[instT.size()];

        // Copiar los elementos de instT a vecT
        for (int j = 0; j < instT.size(); j++) {
            vecT[j] = instT.get(j);
        }
        // Copiar los elementos de instTN a vecTN
        for (int j = 0; j < instTN.size(); j++) {
            vecTN[j] = instTN.get(j);
        }

        for (int i = 2; i < vecTN.length; i++) {
            t = -1;
            if (vecTN[i] == 52) {
                for (int k = 0; k < iTabla; k++) {
                    if (vecT[i].equals(tablaSimbolos[k].getNombreVar())) {
                        t = k;
                    }
                }
                if (t != -1) {
                    if (tablaSimbolos[t].isInicializada()) {
                        tablaSimbolos[t].setUsada(true);
                    } else {
                        String error = "La variable: '" + tablaSimbolos[t].getNombreVar() + "' no esta inicializada";
                        this.errores.add(error);
                    }
                } else {
                    String error = "La variable '" + vecT[i] + "' no existe";
                    this.errores.add(error);
                }
            }
        }
    }

    private void crearTablaSimbolos() {
        int t = 0;
        for (int i = 0; i < noToken.length; i++) {
            if (noToken[i] == 10) {
                t++;
            }   
        }
        this.tablaSimbolos = new Simbolo[t];
    }
}
